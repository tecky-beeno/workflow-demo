import {
  GetWeatherForecast,
  WeatherForecastResult,
} from './ecosystem/GetWeatherForecast'
import { PlaceOrder } from './ecosystem/PlaceOrder'
import { SendEmail } from './ecosystem/SendEmail'
import { EngineService } from './engine-service'
import { WorkFlow } from './models/flow'
import { Task } from './models/task'

let getWeather = new GetWeatherForecast(7)

let workflow1: WorkFlow<WeatherForecastResult> = {
  dependencies: [getWeather],
  async react([forecast]: [WeatherForecastResult]) {
    let tasks: Task[] = []
    if (forecast.rainy > 0.5) {
      let sendEmail = new SendEmail({
        to: 'worker@team.com',
        from: 'it@team.com',
        subject: 'Rain Alert',
        content: 'Today may rain, please bring umbrella when go outdoor',
      })
      tasks.push(sendEmail)
    }
    return tasks
  },
}

let workflow2: WorkFlow<WeatherForecastResult> = {
  dependencies: [getWeather],
  async react([forecast]: [WeatherForecastResult]) {
    let tasks: Task[] = []
    if (forecast.sunny > 0.5) {
      let placeOrder = new PlaceOrder({
        productName: 'water bottle',
        quantity: '1 dozen',
        deliveryAddress: 'main office',
      })
      tasks.push(placeOrder)
    }
    return tasks
  },
}
