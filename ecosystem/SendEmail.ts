import { Task } from '../models/task'
import { createTransport } from 'nodemailer'

export interface SendEmailOptions {
  to: string
  from: string
  subject: string
  content: string
}
export class SendEmail implements Task<void> {
  name: string
  delay = 0
  interval = 0
  transport = createTransport()
  constructor(private options: SendEmailOptions) {
    this.name = 'Send Email with subject: ' + options.subject
  }
  async execute() {
    this.transport.sendMail({
      to: this.options.to,
      from: this.options.from,
      subject: this.options.subject,
      html: this.options.content,
    })
  }
}
