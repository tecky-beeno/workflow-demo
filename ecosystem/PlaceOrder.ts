import fetch from 'node-fetch'
import { Task } from '../models/task'

export interface PlaceOrderOptions {
  productName: string
  quantity: string
  deliveryAddress: string
}
export class PlaceOrder implements Task {
  name: string
  delay = 0
  interval = 0
  constructor(public options: PlaceOrderOptions) {
    this.name = 'place order for ' + options.productName
  }
  async execute() {
    await fetch('https://hktvmall/order', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(this.options),
    })
  }
}
