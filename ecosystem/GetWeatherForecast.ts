import { Task } from '../models/task'
import { DAY } from '../models/time'
import fetch from 'node-fetch'

export interface WeatherForecastResult {
  rainy: number
  sunny: number
}
export class GetWeatherForecast implements Task<WeatherForecastResult> {
  name: string
  delay: number
  interval = DAY
  constructor(hour: number) {
    this.name =
      'Get Weather Forecast at ' +
      hour +
      ' ' +
      (hour < 12 ? 'am' : hour == 12 ? 'nn' : 'pm')
    let next = new Date()
    while (next.getHours() !== hour) {
      next.setHours(next.getHours() + 1, 0, 0, 0)
    }
    let now = Date.now()
    this.delay = next.getTime() - now
  }
  async execute() {
    let res = await fetch('https://hko.gov.hk/weather-forecast')
    let json = await res.json()
    return json
  }
}
