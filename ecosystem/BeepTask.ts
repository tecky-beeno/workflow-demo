import { Task } from '../models/task'

export class BeepTask implements Task<void> {
  name = 'Beep'
  delay = 0
  interval = 1000
  async execute() {
    console.log('beep', new Date().toTimeString())
  }
}
