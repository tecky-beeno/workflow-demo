import { EngineService } from './engine-service'
import { TaskResolver } from './task-resolver'

export class EngineController {
  constructor(
    private engineService: EngineService,
    private taskResolver: TaskResolver,
  ) {}

  async addTaskByName(name: string) {
    let task = await this.taskResolver.resolveTaskByName(name)
    let id = await this.engineService.addTask(task)
    return id
  }

  async removeTaskById(id: number) {
    await this.engineService.removeTask(id)
  }
}
