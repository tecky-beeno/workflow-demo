import { BeepTask } from './ecosystem/BeepTask'
import { Task } from './models/task'

export class TaskResolver {

  async resolveTaskByName(name: string): Promise<Task> {
    switch (name) {
      case 'beep':
        return new BeepTask()
      default:
        throw new Error('unknown task, name: ' + name)
    }
  }
}
