const socket = {
  name: 'fake',
  inputBuffer: [
    'get username',
    'set username alice',
    'set role admin',
    'get username',
    'get role',
    'del username',
    'get role',
    'get username',
    'bye',
  ],
}

function writeLine(socket, line) {
  console.log('send to socket:', line)
}

function readLine(socket, bufferIndex) {
  const line = socket.inputBuffer[bufferIndex]
  // bufferIndex++
  console.log('read from socket:', line)
  return line
}

const handlers = {
  get: function (mem, key) {
    // console.log('select:', rest)
    return [mem, mem[key]]
  },
  set: function (mem, key, value) {
    // console.log('update:', rest)
    const newMem = { ...mem, [key]: value }
    // return 'updated 1 rows'
    return [newMem, `saved ${key} = ${value}`]
  },
  del: function (mem, key) {
    // console.log('delete:', rest)
    const { [key]: value, ...newMem } = mem
    return [newMem, `deleted ${key}`]
  },
}

function handleSocket(mem, socket, bufferIndex) {
  const line = readLine(socket, bufferIndex)
  if (line === 'bye') {
    return
  }
  const [command, ...rest] = line.split(' ')
  const handler = handlers[command]
  const [newMem, result] = handler(mem, ...rest)
  writeLine(socket, result)
  handleSocket(newMem, socket, bufferIndex + 1)
}

const initialMem = {}
handleSocket(initialMem, socket, 0)
console.log('terminated')
