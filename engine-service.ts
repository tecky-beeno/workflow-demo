import { EngineDB } from './engine-db'
import { WorkFlow } from './models/flow'
import { Task } from './models/task'

interface Slot {
  id: number
  task: Task
  timer: ReturnType<typeof setInterval>
}

interface TaskResultListener {
  (result: any): void
}

interface TaskResultListenerV2 {
  onResult(result: any): void
  onEnd(): void
}

export class EngineService {
  activeSlots: Slot[] = []
  nextId = 1

  // task id -> Array<TaskResultListener>
  taskResultListeners: Record<number, TaskResultListener[]> = {}

  ready: Promise<void>

  constructor(private engineDB: EngineDB) {
    this.ready = this.restoreFromDB()
  }

  async restoreFromDB() {}

  hookTaskResult(taskId: number, taskResultListener: (result: any) => void) {
    // 1. check if the task is already executed, if yes, get result from DB

    // 2. add event listener when the task is done
    if (taskId in this.taskResultListeners) {
      this.taskResultListeners[taskId].push(taskResultListener)
    } else {
      this.taskResultListeners[taskId] = [taskResultListener]
    }
  }

  hookTasksResult(
    taskIds: number[],
    taskResultsListener: (results: any[]) => void,
  ) {
    let results: any[] = []
    let gotNResult = 0
    taskIds.forEach((taskId, index) => {
      this.hookTaskResult(taskId, result => {
        if (!(index in results)) {
          gotNResult++
        }
        results[index] = result
        if (gotNResult === taskIds.length) {
          taskResultsListener(results)
        }
      })
    })
  }

  async addFlow(flow: WorkFlow) {
    let depTaskIds = await Promise.all(
      flow.dependencies.map(task => this.addTask(task)),
    )
    this.hookTasksResult(depTaskIds, async deps => {
      let responseTasks = await flow.react(deps)
      let resTaskIds = responseTasks.map(task => this.addTask(task))
      return resTaskIds
    })
  }

  private async executeTaskSlot(slot: Slot) {
    let result = await slot.task.execute()
    this.taskResultListeners[slot.id]?.forEach(listener => listener(result))
  }

  async addTask(task: Task) {
    let id = this.nextId
    this.nextId++
    let slot: Slot = {
      id,
      task,
      timer: setTimeout(async () => {
        if (task.interval) {
          slot.timer = setInterval(() => {
            this.executeTaskSlot(slot)
          }, task.interval)
          return
        }
        await this.executeTaskSlot(slot)
        this.removeTask(id)
      }, task.delay),
    }
    this.activeSlots.push(slot)
    // TODO update in DB
    return id
  }

  async removeTask(id: number) {
    let idx = this.activeSlots.findIndex(slot => slot.id === id)
    if (idx === -1) {
      throw new Error('task not found, id: ' + id)
    }
    let slot = this.activeSlots.splice(idx, 1)[0]
    clearTimeout(slot.timer)
    clearInterval(slot.timer)
    if (id in this.taskResultListeners) {
      // TODO run teardown logics
      delete this.taskResultListeners[id]
    }
    // TODO update in DB
  }
}
