interface ValueChangedEvent<T> {
  index: number
  newValue: T
  oldValue: T
}
interface ValueListener<T> {
  onChange(event: ValueChangedEvent<T>): void
}
export class List<T> {
  // index -> value
  private data: T[]

  // index -> Array<ValueListener<T>>
  private listeners: ValueListener<T>[][] = []

  constructor() {
    this.data = []
  }

  listenValue(index: number, listener: ValueListener<T>) {
    if (index in this.listeners) {
      this.listeners[index].push(listener)
    } else {
      this.listeners[index] = [listener]
    }
  }

  getValue(index: number): T {
    if (index in this.data) {
      return this.data[index]
    }
    throw new Error('Index out of range')
  }

  setValue(index: number, newValue: T) {
    let oldValue = this.data[index]
    let event: ValueChangedEvent<T> = {
      index,
      oldValue,
      newValue,
    }
    this.listeners[index]?.forEach(listener => listener.onChange(event))
    this.data[index] = newValue
  }
}

let strList = new List<string>()
strList.setValue(1, 'ten')
console.log(strList.getValue(1))

let numList = new List<number>()
numList.setValue(1, 10)
console.log(numList.getValue(1))

let listener = {
  onChange(event: ValueChangedEvent<string>) {
    console.log(
      'index:',
      event.index,
      'changed from:',
      event.oldValue,
      '-->',
      event.newValue,
    )
  },
}
strList.listenValue(1, listener)
strList.setValue(2, 'two')
strList.setValue(1, 'eleven')
strList.setValue(1, 'one hundred')
