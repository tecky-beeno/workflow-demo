import { Task } from './task'

export interface WorkFlow<Dep = unknown> {
  dependencies: Task<Dep>[]
  react(deps: Dep[]): Promise<Task[]>
}
