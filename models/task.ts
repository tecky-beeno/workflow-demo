export interface Task<R = unknown> {
  name: string
  delay: number
  interval: number
  execute(): Promise<R>
}
